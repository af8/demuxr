# demuxr package

`demuxr` is a R package that parses a demultiplexing JSON file produced by Illumina `bcl2fastq` software and generates a simple HTML report.

## versions

 - 0.2.7 : removed coverage estimation
 - 0.2.8 : changed HTML display (sections order and a few minor things)
 - 0.2.9 : adaptation for single-end runs

## Dependencies

The following R packages are required to use demuxr : jsonlite, rmarkdown, knitr, magrittr, DT, rmdformats.

## Manual installation

Be sure the dependencies are all installed and then simply run from command line:
```sh
git clone git@gitlab.com:af8/demuxr.git
R CMD build demuxr
R CMD INSTALL demuxr_0.2.9.tar.gz
# Remove source directory if installation was successful
rm -rf demuxr
```

Install step will fail if there are missing dependencies. These need to be installed first. See `DESCRIPTION`.

## Using demuxr package

The only function users should know about is `runDemuxReport`. Classical usage:
```r
library(demuxr)
jsonStatsFile <- "Stats.json"
sampleSheetFile <- "SampleSheet.csv"
runDemuxReport(jsonStatsFile = jsonStatsFile, outDir = ".", sampleSheetFile = sampleSheetFile)
```

This will generate in `outDir` a file named `${runId}-demuxr.qc.html` where `$runId` is the identifier of the run stored in the json stats file.

### Mandatory arguments

There are only one mandatory argument to build the report:
 - jsonStatsFile: Illumina JSON file containing demultiplexing statistics

### Sample Sheet

It is not mandatory but recommended, the Sample Sheet used for demultiplexing can also be supplied through `sampleSheetFile` argument. If so, a supplementary table with the [Data] section of SampleSheet will be added to the "Sample sheet" section of the report. If the SampleSheet has a [Settings] section, it will also be displayed _verbatim_ in the report.

Basically, the information contained in the **Sample_Project** column will be propagated to the main table of the report.

Currently, the SampleSheet parser makes the (almost 100% time true) assumption that [Data] section is the last section of the file.

### Project name

If the **Sample_Project** column of SampleSheet is empty or if you want to overwrite/rename its content, you can fill `projectName` of `runDemuxReport` function. This will use `projectName` value everywhere in the report for every sample.

> This will not modify SampleSheet file content !

### Other arguments

Full set of parameters of `runDemuxReport`:
```r
runDemuxReport(jsonStatsFile = NULL, outDir = getwd(), outFile = NULL, rmdTemplate = NULL, sampleSheetFile = NULL, outputSamplesTable = FALSE, projectName = NULL)
```

 - outDir: the html report will be saved in this directory
 - outFile: customise the output filename of report
 - rmdTemplate: provide your own or a modified template (mainly used in dev/test)
 - outputSamplesTable: save the report main table in a tsv file (also located in outDir)


## Html themes working good with this report

Currently the display is based on `readthedown` theme for which a few custom CSS
modifications has been added.

> At the moment there is no automatic mechanism allowing you to customize/specify a different html theme. You would have to edit the Yaml header of the Rmd template manually and pass it to `rmdTemplate` parameter of `runDemuxReport` function.

### From rmdformats package

 - readthedown
 - material

Yaml header:
```
output: rmdformats::material
```

### From prettydoc package (if package is installed)

 - cayman
 - architect

Yaml header example:
```
output:
  prettydoc::html_pretty:
    theme: architect
```
